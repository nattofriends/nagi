#!/usr/bin/python
# -*- coding: utf-8 -*-

from collections import defaultdict
from os import makedirs, rename
from os.path import basename, dirname, exists, join, realpath
import codecs
import errno
import functools
import pickle
import re
import tempfile
import time
import threading

from queue import Queue
from retrying import retry
import daemon
import requests
import oursql
import pylru
import setproctitle
import ujson

from pidfile import PidFile
from util import *
import config

# URLs
THREAD_LIST = "https://a.4cdn.org/{board}/threads.json"
THREAD = "https://a.4cdn.org/{board}/res/{no}.json"
IMAGE = "https://i.4cdn.org/{board}/src/{tim}{ext}"
IMAGE_THUMB = "https://t.4cdn.org/{board}/thumb/{tim}s.jpg"

# Typed structures
class ThreadLastModifiedEntry(mappable_namedtuple('ThreadLastModifiedEntryBase', ['board', 'last_modified', 'no'])):
    pass

Page = mappable_namedtuple('Page', ['threads', 'page'])
RawThread = mappable_namedtuple('Thread', ['board', 'no', 'json'])
class Post(mappable_namedtuple('PostBase', ['no', 'resto', 'sticky', 'closed', 'now', 'time', 'name', 'trip', \
    'filename', 'id', 'capcode', 'country', 'country_name', 'email', 'sub', 'com', 'tim', \
    'ext', 'fsize', 'md5', 'w', 'h', 'tn_w', 'tn_h', 'filedeleted', 'spoiler', \
    'custom_spoiler', 'omitted_posts', 'omitted_images', 'replies', 'images', \
    'bumplimit', 'imagelimit'])):

    capcodes = {
        None: 'N',
        'mod': 'M',
        'admin': 'A',
        'admin_highlight': 'G',
        'developer': 'A',
    }

    def __eq__(self, other):
        return self.no.__eq__(other.no)

    def __hash__(self):
        return self.no.__hash__()

    def db_repr(self, op):
        parent = 0 if op is self else op.no
        preview = '{tim}s.jpg'.format(tim=self.tim) if self.tim is not None else None
        media = '{tim}{ext}'.format(tim=self.tim, ext=self.ext) if self.tim is not None and self.ext is not None else None

        def clean(text):
            if not text:
                return text

            # Some text escaping
            text = re.sub(r'\[(banned|moot)\]', r'[\1:lit]', text)

            # Code tags
            text = re.sub(r'<pre [^>]*>', r'[code]', text)
            text = re.sub(r'</pre>', r'[/code]', text)

            # Comment too long, exif tag toggle
            text = re.sub(r'<span class="abbr">.*?</span>', r'', text)

            # USER WAS * FOR THIS POST
            text = re.sub(r'<(?:b|strong) style="color:\s*red;">(.*?)</(?:b|strong)>', r'[banned]\1[/banned]', text)

            # moot text
            text = re.sub(r'<div style="padding: 5px;margin-left: \.5em;border-color: #faa;border: 2px dashed rgba\(255,0,0,\.1\);border-radius: 2px">(.*?)</div>', r'[moot]\1[/moot]', text)

            # Bold text
            text = re.sub(r'<(?:b|strong)>(.*?)</(?:b|strong)>', r'[b]\1[/b]', text)

            # Who are you quoting?
            text = re.sub(r'<font class="unkfunc">(.*?)</font>', r'\1', text)
            text = re.sub(r'<span class="quote">(.*?)</span>', r'\1', text)
            text = re.sub(r'<span class="(?:[^"]*)?deadlink">(.*?)</span>', r'\1', text)

            # Get rid of links
            text = re.sub(r'<a[^>]*>(.*?)</a>', r'\1', text)

            # Spoilers
            text = re.sub(r'<span class="spoiler"[^>]*>', r'[spoiler]', text)
            text = re.sub(r'</span>', r'[/spoiler]', text)

            text = re.sub(r'<s>', r'[spoiler]', text)
            text = re.sub(r'</s>', r'[/spoiler]', text)

            # <wbr>
            text = re.sub(r'<wbr>', '', text)

            # Newlines
            text = re.sub(r'<br>', '\n', text)

            text = unescape(text)

            return text

        # id, num, subnum, parent, date, preview, preview_w, preview_h, media, media_w, media_h,
        # media_size, media_hash, media_filename, spoiler, deleted, capcode,
        # email, name, trip, title, comment, delpass, sticky
        return (
            0,
            self.no,
            0,
            parent,
            self.time,
            preview,
            self.tn_w if self.tn_w else 0,
            self.tn_h if self.tn_h else 0,
            self.filename,
            self.w if self.w else 0,
            self.h if self.h else 0,
            self.fsize if self.fsize else 0,
            self.md5,
            media,
            1 if self.spoiler else 0,
            1 if self.filedeleted else 0,
            self.capcodes[self.capcode],
            self.email,
            self.name,
            self.trip,
            self.sub,
            clean(self.com),
            None,
            1 if self.sticky else 0,
        )

Image = mappable_namedtuple('Image', ['url', 'no', 'board', 'type'])

missing_fields = set(Post._fields).difference


def thread_index():
    """Watch thread indices.
    In: None
    Out: ThreadLastModifiedEntry to request_queue
    """
    global active_threads

    while True:
        next_active_threads = defaultdict(dict)

        for board in config.BOARDS:
            url = THREAD_LIST.format(board=board)
            resp = get(url)

            if consider_response("Index", board, resp):
                json = ujson.loads(resp.text)
                pages = map_cast(Page, json)

                for page in pages:
                    # Add some information to the threads
                    [thread.update(board=board) for thread in page.threads]
                    threads = map_cast(ThreadLastModifiedEntry, page.threads)
                    for thread in threads:
                        next_active_threads[board][thread.no] = thread

                        if thread.no in active_threads[board] and active_threads[board][thread.no].last_modified == thread.last_modified:
                            # log.debug("Not modified: {board}/{no}".format(board=board, no=thread.no))
                            continue

                        # Thread was modified, or not yet seen.
                        request_queue.put(thread)
                        log.debug("Added {board}/{no} to queue ({size!s} waiting)".format(board=thread.board, no=thread.no, size=request_queue.qsize()))

        active_threads = next_active_threads
        time.sleep(config.INDEX_REFRESH_INTERVAL)

def request():
    """Requests threads.
    In: ThreadLastModifiedEntry from request_queue
    Out: RawThread to response_queue
    """
    while True:
        item = request_queue.get()
        url = THREAD.format(**item)

        resp = get(url)

        if consider_response("Thread", "{board}/{no}".format(board=item.board, no=item.no), resp, item, request_queue):
            json = ujson.loads(resp.text)
            response_queue.put(RawThread(json=json['posts'], **item))
            log.debug("Downloaded thread {board}/{no} ({size!s} waiting)".format(board=item.board, no=item.no, size=request_queue.qsize()))

def thread(conn):
    """Parses threads, makes posts, finds images to download
    In: RawThread from response_queue
    Out: Image to image_queue
    """
    sql = """
    INSERT INTO {board}
    (id, num, subnum, parent, timestamp, preview, preview_w, preview_h, media, media_w, media_h, media_size, media_hash, media_filename, spoiler, deleted, capcode, email, name, trip, title, comment, delpass, sticky) VALUES
    (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)

    ON DUPLICATE KEY UPDATE
    comment = values(comment), deleted = values(deleted),
    media = coalesce(values(media), media), sticky = (values(sticky) || sticky),
    preview = coalesce(values(preview), preview), preview_w = greatest(values(preview_w), preview_w),
    preview_h = greatest(values(preview_h), preview_h), media_w = greatest(values(media_w), media_w),
    media_h = greatest(values(media_h), media_h), media_size = greatest(values(media_size), media_size),
    media_hash = coalesce(values(media_hash), media_hash), media_filename = coalesce(values(media_filename), media_filename)
    """

    @functools.lru_cache(maxsize=None)
    def sql_for(board):
        return sql.format(board=board)

    cur = conn.cursor()

    while True:
        item = response_queue.get()
        posts = item.json
        [post.update({field: None for field in missing_fields(post)}) for post in posts]
        posts = map_cast(Post, posts)
        log.debug("Writing thread {board}/{no} to database ({size!s} waiting)".format(board=item.board, no=item.no, size=response_queue.qsize()))

        op = posts[0]
        db_values = []

        for post in posts:
            # Read-only should be safe for this and we are protected by GIL
            # already.
            if post in seen_posts:
                continue

            with post_cache_lock:
                seen_posts[post] = 1

            db_values.append((item.board, post.db_repr(op)))

            if post.filename:
                image_url = IMAGE.format(board=item.board, tim=post.tim, ext=post.ext)
                thumb_url = IMAGE_THUMB.format(board=item.board, tim=post.tim)
                image_queue.put(Image(url=image_url, type='img', **item))
                image_queue.put(Image(url=thumb_url, type='thumb', **item))

        for board, db_repr in db_values:
            try:
                cur.execute(sql_for(board), db_repr)
            except oursql.ProgrammingError as e:
                if e.args[0] in (oursql.errnos['ER_LOCK_DEADLOCK'], oursql.errnos['ER_LOCK_WAIT_TIMEOUT']):
                    log.warning("Got transaction lock problem, retrying...")
                    db_values.append((board, db_repr))
                else:
                    raise
            except oursql.OperationalError as e:
                if e.args[0] in (oursql.errnos['CR_SERVER_LOST'], oursql.errnos['CR_SERVER_GONE_ERROR']):
                    log.warning("Lost connection, retrying...")
                    db_values.append((board, db_repr))
                else:
                    raise
            except oursql.CollatedWarningsError as e:
                log.warning("SQL warning: " + str(e))

        conn.commit()

def image():
    """Downloads images.
    In: Image from image_queue
    Out: None
    """
    def subdirs(no):
        format_dir = lambda string_length: '{:0{}d}'.format(int(string_length[0]), string_length[1])
        m = re.match(r"(\d+?)(\d{2})\d{0,3}$", str(no))
        return list(map(format_dir, list(zip(m.groups(), (4, 2)))))

    while True:
        item = image_queue.get()
        path = join(config.DATA_DIR, item.board, item.type, *subdirs(item.no))

        if exists(join(path, basename(item.url))):
            continue

        image = get(item.url)
        log.debug("Downloading image at {url} ({size!s} waiting)".format(url=item.url, size=image_queue.qsize()))

        if consider_response("Image", item.url, image, item, image_queue):
            if not exists(path):
                try:
                    makedirs(path)
                except OSError as e:
                    if e.errno == errno.EEXIST:
                        pass
                    else:
                        raise

            with open(join(path, basename(item.url)), 'wb') as image_file:
                image_file.write(image.content)

def stats():
    """Provides queue size information.
    """
    global active_threads

    while True:
        log.info("Queue sizes: request/{} response/{} image/{}".format(request_queue.qsize(), response_queue.qsize(), image_queue.qsize()))
        setproctitle.setproctitle("nagi: request/{} response/{} image/{}".format(request_queue.qsize(), response_queue.qsize(), image_queue.qsize()))

        if config.STATE_FILE_NAME:
            with tempfile.NamedTemporaryFile('wb', dir=dirname(config.STATE_FILE_NAME), delete=False) as state_file:
                state = {
                    'active_threads': active_threads,
                    'seen_posts': list(seen_posts.keys()),
                }
                pickle.dump(state, state_file)
                temp_name = state_file.name
            rename(temp_name, config.STATE_FILE_NAME)

        time.sleep(config.STATS_INTERVAL)

def get(url, headers=None, *args, **kwargs):
    retry_get = retry(
        wait='exponential_sleep',
        wait_exponential_multiplier=1000,
        wait_exponential_max=16000,
        retry_on_exception=lambda exc: isinstance(exc, requests.exceptions.RequestException),
    )(requests.get)

    last_seen = seen_urls[url] if url in seen_urls else None
    headers = {} if not headers else headers

    if last_seen:
        headers['If-Modified-Since'] = last_seen

    resp = retry_get(url, headers=headers, *args, **kwargs)

    # The less writing, the better!
    if not last_seen:
        with url_cache_lock:
            seen_urls[url] = time.strftime('%a, %d %b %Y %H:%M:%S GMT', time.gmtime(time.time()))

    return resp

def consider_response(type, name, resp, item=None, queue=None):
    if resp.status_code == requests.codes.not_modified:
        resp.close()
        return False
    if resp.status_code == requests.codes.not_found:
        log.warning("{type} {name} is not found, not retrying".format(type=type, name=name))
        resp.close()
        return False
    elif resp.status_code in (
        requests.codes.server_error,
        requests.codes.service_unavailable,
    ):
        log.warning("{type} {name} failed to download (error {status_code}), retrying".format(type=type, name=name, status_code=resp.status_code))

        resp.close()
        if item and queue:
            queue.put(item)

        return False
    return True

def run_workers():
    codecs.register(lambda name: codecs.lookup('utf8') if name == 'utf8mb4' else None)

    conn = lambda: oursql.connect(host=config.DB_HOST, user=config.DB_USER, passwd=config.DB_PASSWORD, db=config.DB_NAME, charset='utf8mb4', autoping=True)
    curs = [conn() for conn in [conn] * config.DB_WRITE_THREADS]

    request_workers = [threading.Thread(target=request) for i in range(config.REQUEST_DOWNLOAD_THREADS)]
    thread_workers = [threading.Thread(target=thread, args=(cur,)) for cur in curs]
    image_workers = [threading.Thread(target=image) for i in range(config.IMAGE_DOWNLOAD_THREADS)]
    other_workers = [threading.Thread(target=worker) for worker in [thread_index, stats]]

    workers = request_workers + thread_workers + image_workers + other_workers
    log.info("Using {num} threads".format(num=len(workers)))

    for worker in workers:
        worker.daemon = True
        worker.start()

    try:
        [worker.join() for worker in workers]
    except KeyboardInterrupt:
        log.info("Received ^C, shutting down")
        sys.exit(0)


if __name__ == "__main__":
    log, log_file = init_logging()

    log.info("nagi starting")

    active_threads = defaultdict(dict)
    url_cache_lock = threading.Lock()
    post_cache_lock = threading.Lock()
    seen_urls = pylru.lrucache(config.HTTP_STAMP_CACHE_SIZE)
    seen_posts = pylru.lrucache(config.POST_CACHE_SIZE)
    request_queue = Queue()
    response_queue = Queue()
    image_queue = Queue()

    if config.STATE_FILE_NAME and exists(config.STATE_FILE_NAME):
        try:
            with open(config.STATE_FILE_NAME, 'rb') as state_file:
                state = pickle.load(state_file)

                log.info("Restoring seen post state")

                active_threads = state['active_threads']
                for seen_post in state['seen_posts']:
                    seen_posts[seen_post] = 1
        except:
            pass

    if config.DAEMON:
        log.info("Daemonizing...")

        if config.PID_FILE_NAME:
            pidfile = PidFile(log, config.PID_FILE_NAME)
        else:
            pidfile = None

        with daemon.DaemonContext(
            pidfile=pidfile,
            stderr=log_file,
            files_preserve=[pidfile.pidfile],
        ):
            run_workers()
    else:
        run_workers()
