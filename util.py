#!/usr/bin/python
# -*- coding: utf-8 -*-

from collections import Mapping, namedtuple
from html.parser import HTMLParser
import logging
import sys

from rainbow_logging_handler import RainbowLoggingHandler

import config

def mappable_namedtuple(name, fields):
    """Usually, a namedtuple's fields can only be accessed by property through
    its field name (point.x) or by index into its field's order (point[1]).
    mappable_namedtuples can also be accessed by index into the field name (point["x"]).
    This makes them suitable for direct use in string formatting.
    """

    @staticmethod
    def new(cls, *args, **kwargs):
        return super(mappable, cls).__new__(cls, *args, **{k: v for k, v in list(kwargs.items()) if k in cls._fields})

    def getitem(self, key):
        if isinstance(key, int):
            return super(mappable, self).__getitem__(key)
        elif isinstance(key, str):
            return super(mappable, self).__getattribute__(key)

    backing = namedtuple(name, fields)
    mappable = type(name, (backing, Mapping), {'__dict__': backing.__dict__})

    mappable.keys = lambda self: self._fields
    mappable.__new__ = new
    mappable.__getitem__ = getitem
    return mappable


def map_cast(cls, iter):
    """Cast all items in a iterable of dicts to cls."""
    return [cls(**dict_) for dict_ in iter]


def init_logging():
    log = logging.getLogger("nagi")
    log.setLevel(config.LOGGING_LEVEL)

    handlers = []
    log_file = None

    if config.LOG_FILE_NAME:
        log_file = open(config.LOG_FILE_NAME, 'a')
        if not config.DAEMON:
            handlers.append(logging.StreamHandler(log_file))

    handlers.append(RainbowLoggingHandler(sys.stderr, datefmt=None))

    formatter = logging.Formatter('%(asctime)s [%(levelname)s] %(message)s')

    for handler in handlers:
        handler.setFormatter(formatter)
        log.addHandler(handler)

    return log, log_file

unescape = HTMLParser().unescape
