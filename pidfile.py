import os
import sys

import psutil

class PidFile(object):
    """fcntl sucks. Cry and look at the content of the pidfile instead.
    """

    def __init__(self, logger, path):
        self.logger = logger
        self.path = path
        self.pidfile = None

    def __enter__(self):
        if not os.path.exists(self.path):
            with open(self.path, 'w'):
                pass

        self.pidfile = open(self.path, "r+")

        existing_pid = self.pidfile.read()

        try:
            existing_pid = int(existing_pid)
            existing_process = psutil.Process(existing_pid)
            self.logger.error("An instance (pid {pid}) is already running, shutting down this pid {this_pid}. Pid file is located at {pidfile}".format(
                pid=existing_pid,
                this_pid=os.getpid(),
                pidfile=self.path,
            ))
            sys.exit(-1)
        except ValueError:
            pass
        except psutil.NoSuchProcess:
            pass

        self.pidfile.seek(0)
        self.pidfile.truncate()
        self.pidfile.write(str(os.getpid()))
        self.pidfile.flush()
        self.pidfile.seek(0)
        return self.pidfile

    def __exit__(self, exc_type=None, exc_value=None, exc_tb=None):
        try:
            self.pidfile.close()
        except IOError as err:
            # ok if file was just closed elsewhere
            if err.errno != 9:
                raise
        os.remove(self.path)
